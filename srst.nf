nextflow.enable.dsl = 2

params.outdir = "./out3/"
params.fastq_infiles = "./rawdata/*.fastq" // Referenzsequenz
params.resistance_gene_db = "./CARD_v3.0.8_SRST2.fasta"


process srst2{
publishDir "${params.outdir}", mode: "copy" , overwrite: true // erschafft nur Ordner
    container "https://depot.galaxyproject.org/singularity/srst2%3A0.2.0--py27_2"
    input:
        path fastq_infiles //oder fastp?
        path resistance_gene_db
        //value tuple
    output:
        path "*" // nur die _txt results aus srst2 in eine Datei
    script: 
    """
    srst2 --input_se ${fastq_infiles} --output ${fastq_infiles}_results.txt --log --gene_db ${resistance_gene_db}
    """
}

workflow {
  fastq_inchannel = channel.fromPath(params.fastq_infiles)
  //fastq_inchannel.view()
  resistance_gene_db_inchannel = channel.fromPath(params.resistance_gene_db)
  
  
  
  //resistance_gene_db_inchannel.view()
  //srst2(fastq_inchannel,resistance_gene_db_inchannel)
  
 
 
  //combine_channel = fastq_inchannel.combine(resistance_gene_db_inchannel)
  //resistance_prediction_channel = srst2(combine_channel)



  fastq_inchannel.combine(resistance_gene_db_inchannel).multiMap{it //LSg
                                                                    fastq: it[0] // lsg
                                                                    db: it[1]} //lsg
                                                       .set{tuple} //lsg
  //tuple.fastq.view()
  //tuple.db.view()
  srst2(tuple.fastq,tuple.db) //Lsg.
  
  
 // combine_channel = fastq_inchannel.combine(resistance_gene_db_inchannel)
  //empty_channel = channel.empty()
  //srst2(combine_channel,empty_channel)

  
}


// nextflow run srst.nf -profile singularity 



















