nextflow.enable.dsl = 2


process fastp { //Trimming
    publishDir "${params.outdir}", mode: "copy" , overwrite: true 
    container "https://depot.galaxyproject.org/singularity/fastp:0.22.0--h2e03b76_0"
    input:
        path fastq_infile
    output:
        path "${fastq_infile.getSimpleName()}_trim.fastq" 
    script: 
    """
    fastp -i ${fastq_infile} -o ${fastq_infile.getSimpleName()}_trim.fastq 
    """
}


process fastqc{ // Generierung eines Qualitätsreports über die getrimmten Daten
    publishDir "${params.outdir}", mode: "copy" , overwrite: true 
    container "https://depot.galaxyproject.org/singularity/fastqc%3A0.11.9--hdfd78af_1"
    input:
        path fastq_trim_infile
    output:
        path "fastQC_results*" 
    script: 
    """
    mkdir fastQC_results
    fastqc -o ./fastQC_results ${fastq_trim_infile}
    """
}


process srst2{ // Resistenzvorhersage mittels srst2
publishDir "${params.outdir}", mode: "copy" , overwrite: true
    container "https://depot.galaxyproject.org/singularity/srst2%3A0.2.0--py27_2"
    input:
        path fastq_trim_infile
        path resistance_gene_db
    output:
        path "*" // nur die _txt results aus srst2 in eine Datei
    script: 
    """
    srst2 --input_se ${fastq_trim_infile} --output ${fastq_trim_infile}_results.txt --log --gene_db ${resistance_gene_db}
    """
}


workflow {
  fastq_inchannel = channel.fromPath("${params.indir}/*fastq")
  resistance_gene_db_inchannel = channel.fromPath(params.resistance_gene_db)
  fastp_channel = fastp(fastq_inchannel.flatten())
  fastqc_channel = fastqc(fastp_channel.collect())
  
  //combine_channel = fastq_inchannel.combine(resistance_gene_db_inchannel)
  
  
  fastp_channel.combine(resistance_gene_db_inchannel).multiMap{ it ->
                                                                fastq: it[0]
                                                                db: it[1]
                                                              }
                                                     .set{tuple}
  //tuple.fastq.view()
  //tuple.db.view()
  srst2(tuple.fastq,tuple.db)
  //resistance_prediction_channel = srst2(combine_channel)
  //resistance_prediction_channel = srst2(fastq_inchannel, params.resistance_gene_db_inchannel) // kann ich inchannel 2 mal verwenden? ja nur Prozess nicht 2 mal
}
//Aufruf in Konsole: nextflow run Prüfungsaufgabe_Martina_Schedler.nf --indir ~/ngsmodule/Abschlussaufgabe/rawdata --outdir ./out -profile singularity --resistance_gene_db CARD_v3.0.8_SRST2.fasta 




